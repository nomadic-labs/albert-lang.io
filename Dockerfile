FROM jfloff/alpine-python as builder

RUN apk --no-cache update && apk --no-cache add make

WORKDIR /tempDir
COPY . /tempDir

RUN pip3 install --upgrade pip && pip3 install -r requirements.txt

RUN make index.html

FROM nginx:alpine
COPY --from=builder /tempDir/index.html /tempDir/styles.css /tempDir/*.png  /tempDir/*.ico /usr/share/nginx/html/
COPY --from=builder /tempDir/files/* /usr/share/nginx/html/files/
