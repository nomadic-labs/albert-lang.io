Website for the Albert language.

## Editing the website

The source for the website is in `index.md`.

## Verifying examples

```
$ ./check_albert.sh
```

requires `albert_c` in `PATH`.

## Deploying the website

### Testing locally

    make

### Testing using Docker

    docker build -t nomadiclabs/albert_website:v1 .
    docker run -d -p 80:80 nomadiclabs/albert_website:v1

### In the Wild

    docker build -t nomadiclabs/albert_website .
    docker push nomadiclabs/blog
