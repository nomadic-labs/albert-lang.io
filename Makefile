index.html: index.md template.html publish.py
	python3 publish.py $< > $@

.PHONY: check_examples
check_examples:
	./check_albert.sh


.PHONY: check_links
check_links:
	linkchecker -r0 `grep -oe  'http.[^ )]*' index.md`

.PHONY: check
check: check_examples check_links

docker-push:
	docker build -t nomadiclabs/albert_website .
	docker push nomadiclabs/albert_website
