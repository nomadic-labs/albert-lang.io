#!/bin/bash

set -e

if [ ! $( which albert_c ) ]; then
   echo "Found no albert_c in PATH."
   exit 1
fi

TEMPDIR=/tmp/albert-temp
mkdir -p $TEMPDIR
trap "rm -rf $TEMPDIR" EXIT


sed -n '/^```python/,/^```/ p' < index.md > $TEMPDIR/examples.alb
cd $TEMPDIR
csplit --suppress-matched --elide-empty-files  --digits=2 --quiet --prefix=example \
       --suffix=%d.alb examples.alb "/\`\`\`/" "{*}" "/\`\`\`python/"
rm examples.alb

for i in `ls *.alb`; do
    if ! albert_c $i; then
        echo "Compilation of $i failed: ";
        cat $i
        exit 1
    else
        echo "Compilation of $i successful.";
    fi
done

echo "All examples compile."
